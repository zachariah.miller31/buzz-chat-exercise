# Chat program exercise instructions

For this exercise there will be two parts. One, identify how to connect to the already hosted server for this chat program located at http://staticphantom.com:8003/ and two, modify the relevant code in the home.html to display the chat like an im/messenger window, with users on each side of the window (and whatever else you want if you want to get fancy). To simplify this and demonstrate the ability to work with 3rd party components, please utilize [Materialize](https://materializecss.com/) web components for this. 

The instructions on the website are very simple to get started you mearly need to include the below tags in the appropriate sections of the home.html.

```html
 <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
```

### Exercise Assignment items
Providing bulleted list of items here for clarity:
1. Create a gitlab.com account
1. Request access to this repository you are viewing
1. Download golang so local build will work (see steps below)
1. install git so you can pull down the project and make your submission
1. Download and build code
1. Update home.html to point to chat server hosted at http://staticphantom.com:8003. (hint: this is a 1 line or less change)
1. Add the provided stylesheet and script tags for materialize to the home.html
1. Update the client application using css/javascript/html to display the chat window in an IM like fashion. I'm not asking you to build to a certain spec or style, just do something that looks clean/appealing to you and demonstrates your ability to do so. You can use ``` /nick <name> ``` in the chat to select/assign your "name".
1. Bonus: I in no way expect you to do this, but if you enjoy picking stuff apart and want to play around, you are welcome to modify any of the golang code if you want a challenge or see a way to better improve waht you are trying to do by modifying the code there, but that is above and beyond the intent of this exercise, which is just to demonstrate what you can do within the home.html using html/css/js

## Notes: 
This is a very simple program, which is ideal for the purposes of this exercise. You should be able to build this project locally and test running the steps below (Setup Instructions). 

If you have issues with this that last more than like 15 min, please feel free to reach out to me at the email provided to you as that was not the intention of the exercise and i will try to help you get the environment set up.


# Setup Instructions

## Install golang, git and clone the repository
1. If you do not have git installed, please install it. It can be found [here](https://git-scm.com/downloads)
1. Navigate to https://golang.org/doc/install
1. Download the installer for your operating system and install as per the instructions on the page for your OS
1. Open a terminal window (on Mac type terminal in the finder on windows open powershell from the start menu)
1. run ```go version```... If installed correctly this command should return the version of golang that was just installed. If you get an error please reach out to me and i'll help resolve. 
1. Clone down the repo for the trial exercise from [here](https://gitlab.com/zachariah.miller31/buzz-chat-exercise)
1. Change to the directory of the newly cloned repository
1. Create a new branch and name it in the format <initials>-exercise. For example: zm-exercise

## Build project
If you're new to go... set up go path. Once set up, first time builds in the checked out folder are this simple:

```bash
$ go mod init main
$ go mod tidy
$ go build
$ ./main
or on windows:
$ .\main.exe
```

Now you can navigate to [http://localhost:8080](http://localhost:8080). To change name in the window, simply 
use `/nick {your_new_handle}` and voila, you're ready to rock. 

To build changes, just `go build` for any modifications. 

## Submission
1. To submit your code, please push the branch you have created to the repository. You can verify this was successful by navigating to the gitlab page and clicking the dropdown below the project name and description that defaults to "master". When you open the menu you should see the branch you created.
